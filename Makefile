.PHONY: usage
usage:
	@echo "========="
	@echo "||USAGE||"
	@echo "========="
	@echo "usage: display usage info"
	@echo "init: initialize the project hierarchy"
	@echo "build: build all projects"
	@echo "build-serial: build serial project"
	@echo "build-openmp: build OpenMP project"
	@echo "build-mpi: build MPI project"
	@echo "build-hybrid-mpi: build hybrid MPI project"
	@echo "benchmark: benchmark all projects"
	@echo "benchmark-serial: benchmark serial project"
	@echo "benchmark-openmp: benchmark OpenMP project"
	@echo "benchmark-mpi: benchmark MPI project"
	@echo "benchmark-hybrid-mpi: benchmark hybrid MPI project"
	@echo "clean: clean all build artifacts"
	@echo "clean-serial: clean serial build artifacts"
	@echo "clean-openmp: clean OpenMP build artifacts"
	@echo "clean-mpi: clean mpi build artifacts"
	@echo "clean-pthreads: clean Pthreads build artifacts"
	@echo "clean-hybrid-mpi: clean hybrid MPI build artifacts"

##########
# COMMON #
##########

SOURCE_DIRECTORY=src
BUILD_DIRECTORY=build
BINARY_DIRECTORY=bin

.PHONY: init-common
init-common:
	@mkdir --parents $(BUILD_DIRECTORY) $(BINARY_DIRECTORY)

SOURCE_SUFFIX=c
HEADER_SUFFIX=h
OBJECT_SUFFIX=obj
LOCK_SUFFIX=lock

COMMON_SOURCE_FILES=$(shell ls $(SOURCE_DIRECTORY)/*.$(SOURCE_SUFFIX))
COMMON_OBJECT_FILES=$(patsubst $(SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX),$(BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX),$(COMMON_SOURCE_FILES))

BINARY_NAME=word_frequency
PROJECT_OUTPUT_BASE_NAME=$(BINARY_DIRECTORY)/$(BINARY_NAME)

COMPILER=gcc
COMPILER_OPTIONS=-std=c17 -Wall -Wextra -Wno-main -Werror -fmax-errors=1 -g

$(COMMON_OBJECT_FILES):\
	$(BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX):\
	$(SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX)
	$(COMPILER) $(COMPILER_OPTIONS) -c $^ --output $@

.PHONY: clean-common
clean-common:
	@rm -f $(COMMON_OBJECT_FILES)

##########
# Serial #
##########

SERIAL_DIRECTORY=serial
SERIAL_SOURCE_DIRECTORY=$(SOURCE_DIRECTORY)/$(SERIAL_DIRECTORY)
SERIAL_BUILD_DIRECTORY=$(BUILD_DIRECTORY)/$(SERIAL_DIRECTORY)

.PHONY: init-serial
init-serial:
	@mkdir --parents $(SERIAL_BUILD_DIRECTORY)

SERIAL_SOURCE_FILES=$(shell ls $(SERIAL_SOURCE_DIRECTORY)/*.$(SOURCE_SUFFIX))
SERIAL_OBJECT_FILES=$(patsubst $(SERIAL_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX),$(SERIAL_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX),$(SERIAL_SOURCE_FILES))

OUTPUT_SERIAL=$(PROJECT_OUTPUT_BASE_NAME)_serial

.PHONY: build-serial
build-serial: $(OUTPUT_SERIAL)

$(OUTPUT_SERIAL): $(SERIAL_OBJECT_FILES) $(COMMON_OBJECT_FILES)
	$(COMPILER) $(COMPILER_OPTIONS) $^ --output $@

$(SERIAL_OBJECT_FILES):\
	$(SERIAL_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX):\
	$(SERIAL_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX)
	$(COMPILER) $(COMPILER_OPTIONS) -c $^ --output $@

benchmark-serial: build-serial
	@echo "Benchmark serial algorithm"
	@bash -c "time $(OUTPUT_SERIAL) $(INPUT_FILE)"

.PHONY: clean-serial
clean-serial:
	@rm -f $(SERIAL_OBJECT_FILES)

##########
# OPENMP #
##########

OPENMP_DIRECTORY=openmp
OPENMP_SOURCE_DIRECTORY=$(SOURCE_DIRECTORY)/$(OPENMP_DIRECTORY)
OPENMP_BUILD_DIRECTORY=$(BUILD_DIRECTORY)/$(OPENMP_DIRECTORY)

.PHONY: init-openmp
init-openmp:
	@mkdir --parents $(OPENMP_BUILD_DIRECTORY)

OPENMP_SOURCE_FILES=$(shell ls $(OPENMP_SOURCE_DIRECTORY)/*.$(SOURCE_SUFFIX))
OPENMP_OBJECT_FILES=$(patsubst $(OPENMP_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX),$(OPENMP_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX),$(OPENMP_SOURCE_FILES))

OUTPUT_OPENMP=$(PROJECT_OUTPUT_BASE_NAME)_openmp

COMPILER_OPENMP=gcc
COMPILER_OPTIONS_OPENMP=$(COMPILER_OPTIONS) -fopenmp

.PHONY: build-openmp
build-openmp: $(OUTPUT_OPENMP)

$(OUTPUT_OPENMP): $(OPENMP_OBJECT_FILES) $(COMMON_OBJECT_FILES)
	$(COMPILER_OPENMP) $(COMPILER_OPTIONS_OPENMP) $^ --output $@

$(OPENMP_OBJECT_FILES):\
	$(OPENMP_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX):\
	$(OPENMP_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX)
	$(COMPILER_OPENMP) $(COMPILER_OPTIONS_OPENMP) -c $^ --output $@

benchmark-openmp: build-openmp
	@echo "Benchmark OpenMP algorithm"
	@bash -c "time $(OUTPUT_OPENMP) $(INPUT_FILE) $(NUMBER_THREADS)"

.PHONY: clean-openmp
clean-openmp:
	@rm -f $(OPENMP_OBJECT_FILES)

#######
# MPI #
#######

MPI_DIRECTORY=mpi
MPI_SOURCE_DIRECTORY=$(SOURCE_DIRECTORY)/$(MPI_DIRECTORY)
MPI_BUILD_DIRECTORY=$(BUILD_DIRECTORY)/$(MPI_DIRECTORY)

init-mpi:
	@mkdir --parents $(MPI_BUILD_DIRECTORY)

MPI_SOURCE_FILES=$(shell ls $(MPI_SOURCE_DIRECTORY)/*.$(SOURCE_SUFFIX))
MPI_OBJECT_FILES=$(patsubst $(MPI_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX),$(MPI_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX),$(MPI_SOURCE_FILES))

OUTPUT_MPI=$(PROJECT_OUTPUT_BASE_NAME)_mpi

COMPILER_MPI=mpicc
COMPILER_OPTIONS_MPI=-std=c17 -Wall -Wextra -Wno-main -Werror -fmax-errors=1 -g

.PHONY: build-mpi
build-mpi: $(OUTPUT_MPI)

$(OUTPUT_MPI): $(MPI_OBJECT_FILES) $(COMMON_OBJECT_FILES)
	$(COMPILER_MPI) $(COMPILER_OPTIONS_MPI) $^ --output $@

$(MPI_OBJECT_FILES):\
	$(MPI_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX):\
	$(MPI_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX)
	$(COMPILER_MPI) $(COMPILER_OPTIONS_MPI) -c $^ --output $@

benchmark-mpi: build-mpi
	@echo "Benchmark MPI algorithm"
	@bash -c "module load mpi/openmpi && time mpirun -np $(NUMBER_NODES) $(OUTPUT_MPI) $(INPUT_FILE)"

.PHONY: clean-mpi
clean-mpi:
	@rm -f $(MPI_OBJECT_FILES)

############
# PTHREADS #
############

PTHREADS_DIRECTORY=pthreads
PTHREADS_SOURCE_DIRECTORY=$(SOURCE_DIRECTORY)/$(PTHREADS_DIRECTORY)
PTHREADS_BUILD_DIRECTORY=$(BUILD_DIRECTORY)/$(PTHREADS_DIRECTORY)

.PHONY: init-pthreads
init-pthreads:
	@mkdir --parents $(PTHREADS_BUILD_DIRECTORY)

PTHREADS_SOURCE_FILES=$(shell ls $(PTHREADS_SOURCE_DIRECTORY)/*.$(SOURCE_SUFFIX))
PTHREADS_OBJECT_FILES=$(patsubst $(PTHREADS_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX),$(PTHREADS_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX),$(PTHREADS_SOURCE_FILES))

OUTPUT_PTHREADS=$(PROJECT_OUTPUT_BASE_NAME)_pthreads

COMPILER_PTHREADS=$(COMPILER)
COMPILER_OPTIONS_PTHREADS=$(COMPILER_OPTIONS) -lpthread

.PHONY: build-pthreads
build-pthreads: $(OUTPUT_PTHREADS)

$(OUTPUT_PTHREADS): $(PTHREADS_OBJECT_FILES) $(COMMON_OBJECT_FILES)
	$(COMPILER_PTHREADS) $(COMPILER_OPTIONS_PTHREADS) $^ --output $@

$(PTHREADS_OBJECT_FILES):\
	$(PTHREADS_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX):\
	$(PTHREADS_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX)
	$(COMPILER_PTHREADS) $(COMPILER_OPTIONS_PTHREADS) -c $^ --output $@

benchmark-pthreads: build-pthreads
	@echo "Benchmark Pthreads algorithm"
	@bash -c "time $(OUTPUT_PTHREADS) $(INPUT_FILE) $(NUMBER_THREADS)"

.PHONY: clean-pthreads
clean-pthreads:
	@rm -f $(PTHREADS_OBJECT_FILES)

##############
# HYBRID MPI #
##############

HYBRID_MPI_DIRECTORY=hybrid_mpi
HYBRID_MPI_SOURCE_DIRECTORY=$(SOURCE_DIRECTORY)/$(HYBRID_MPI_DIRECTORY)
HYBRID_MPI_BUILD_DIRECTORY=$(BUILD_DIRECTORY)/$(HYBRID_MPI_DIRECTORY)

.PHONY: init-hybrid-mpi
init-hybrid-mpi:
	@mkdir --parents $(HYBRID_MPI_BUILD_DIRECTORY)

HYBRID_MPI_SOURCE_FILES=$(shell ls $(HYBRID_MPI_SOURCE_DIRECTORY)/*.$(SOURCE_SUFFIX))
HYBRID_MPI_OBJECT_FILES=$(patsubst $(HYBRID_MPI_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX),$(HYBRID_MPI_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX),$(HYBRID_MPI_SOURCE_FILES))

OUTPUT_HYBRID_MPI=$(PROJECT_OUTPUT_BASE_NAME)_hybrid_mpi

COMPILER_HYBRID_MPI=$(COMPILER_MPI)
COMPILER_OPTIONS_HYBRID_MPI=$(COMPILER_OPTIONS_MPI) -fopenmp

.PHONY: build-hybrid-mpi
build-hybrid-mpi: config-hybrid-mpi $(OUTPUT_HYBRID_MPI)

HYBRID_MPI_CONFIG=$(HYBRID_MPI_SOURCE_DIRECTORY)/config.$(HEADER_SUFFIX)
HYBRID_MPI_CONFIG_LOCK=$(HYBRID_MPI_SOURCE_DIRECTORY)/.config.$(LOCK_SUFFIX)

config-hybrid-mpi: $(HYBRID_MPI_CONFIG_LOCK)

$(HYBRID_MPI_CONFIG_LOCK): $(HYBRID_MPI_CONFIG)
	rm -f $(HYBRID_MPI_OBJECT_FILES) $(OUTPUT_HYBRID_MPI)
	touch $(HYBRID_MPI_CONFIG_LOCK)

$(OUTPUT_HYBRID_MPI):\
	$(HYBRID_MPI_OBJECT_FILES)\
	$(COMMON_OBJECT_FILES)\
	$(OPENMP_BUILD_DIRECTORY)/openmp_algorithm.$(OBJECT_SUFFIX)\
	$(PTHREADS_BUILD_DIRECTORY)/pthreads_algorithm.$(OBJECT_SUFFIX)
	$(COMPILER_HYBRID_MPI) $(COMPILER_OPTIONS_HYBRID_MPI) $^ --output $@

$(HYBRID_MPI_OBJECT_FILES):\
	$(HYBRID_MPI_BUILD_DIRECTORY)/%.$(OBJECT_SUFFIX):\
	$(HYBRID_MPI_SOURCE_DIRECTORY)/%.$(SOURCE_SUFFIX)
	$(COMPILER_HYBRID_MPI) $(COMPILER_OPTIONS_HYBRID_MPI) -c $^ --output $@

benchmark-hybrid-mpi: build-hybrid-mpi
	@echo "Benchmark hybrid MPI algorithm"
	@bash -c "module load mpi/openmpi && time mpirun -np $(NUMBER_NODES) $(OUTPUT_HYBRID_MPI) $(INPUT_FILE) $(NUMBER_THREADS)"

.PHONY: clean-hybrid-mpi
clean-hybrid-mpi:
	@rm -f $(HYBRID_MPI_OBJECT_FILES) $(HYBRID_MPI_CONFIG_LOCK)

#############
# FRONT-END #
#############

.PHONY: display-start-init
display-start-init:
	@echo "Initializing the project..."

.PHONY: display-end-init
display-end-init:
	@echo "Done"

.PHONY: init
init:\
	display-start-init\
	init-common init-serial\
	init-openmp\
	init-mpi\
	init-pthreads\
	init-hybrid-mpi\
	display-end-init

.PHONY: display-start-build
display-start-build:
	@echo "Starting building all artifacts"

.PHONY: display-end-build
display-end-build:
	@echo "Done"

.PHONY: build
build:\
	display-start-build\
	build-serial\
	build-openmp\
	build-mpi\
	build-pthreads\
	build-hybrid-mpi\
	display-end-build

.PHONY: display-start-benchmarks
display-start-benchmarks:
	@echo "Benchmarking all binaries"

.PHONY: display-end-benchmarks
display-end-benchmarks:
	@echo "Done"

.PHONY: benchmark
benchmark:\
	display-start-benchmarks\
	benchmark-serial\
	benchmark-openmp\
	benchmark-mpi\
	benchmark-pthreads\
	benchmark-hybrid-mpi\
	display-end-benchmarks

.PHONY: display-start-clean
display-start-clean:
	@echo "Cleaning build artifacts"

.PHONY: display-end-clean
display-end-clean:
	@echo "Done"

.PHONY: clean
clean:\
	display-start-clean\
	clean-common\
	clean-serial\
	clean-openmp\
	clean-mpi\
	clean-hybrid-mpi\
	display-end-clean
	@rm -f $(BINARY_DIRECTORY)/*
