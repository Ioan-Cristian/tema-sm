#ifndef __FILE_H__
#define __FILE_H__

#include "panic.h"
#include "string_slice.h"

static inline FILE *__open_file(
	const char *const file_path,
	const char *const mode,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	FILE *const file = fopen(file_path, mode);
	if (!file)
		__panic(compilation_unit, line_number, function_name, "couldn't open file %s with mode %s\n", file_path, mode);

	return file;
}

const_string_slice_t __read_file(
	FILE *const file,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
);

static inline const_string_slice_t __get_file_content(
	const char *const file_name,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	FILE *const file =__open_file(file_name, "r", compilation_unit, line_number, function_name);
	const_string_slice_t file_content = __read_file(file, compilation_unit, line_number, function_name);
	fclose(file);

	return file_content;
}

#define get_file_content(file_name) \
	__get_file_content(file_name, __FILE__, __LINE__, __func__)

#endif // __FILE_H__
