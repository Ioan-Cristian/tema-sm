#ifndef __STRING_SLICE_LIST_H__
#define __STRING_SLICE_LIST_H__

#include "config.h"
#include "string_slice.h"

typedef struct {
	const_string_slice_t slices[__MAX_NUMBER_THREADS__];
	const unsigned size;
} const_string_slice_list_t;

const_string_slice_list_t unsafe_partition_slice(const_string_slice_t slice, const unsigned number_threads);

#endif // __STRING_SLICE_LIST_H__
