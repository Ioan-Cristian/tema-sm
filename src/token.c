#include "token.h"

#include <stdbool.h>

static inline bool is_iterator_consumed(const token_iterator_t *const token_iterator) {
	return token_iterator->cursor == token_iterator->end;
}

static inline void advance_token_iterator(token_iterator_t *const token_iterator) {
	++(token_iterator->cursor);
}

#include <ctype.h>

static inline bool is_token_delimiter(const char character) {
	return !isalnum(character);
}

const_token_t next_token(token_iterator_t *token_iterator) {
	if (is_iterator_consumed(token_iterator)) goto token_not_found;

	while(!is_iterator_consumed(token_iterator) && is_token_delimiter(*token_iterator->cursor))
		advance_token_iterator(token_iterator);
	if (is_iterator_consumed(token_iterator)) goto token_not_found;

	const char *token_start = token_iterator->cursor;
	while(!is_token_delimiter(*token_iterator->cursor) && !is_iterator_consumed(token_iterator))
		advance_token_iterator(token_iterator);
	return (const_token_t){.pointer = token_start, .size = token_iterator->cursor - token_start};
token_not_found:
		return (const_token_t){.pointer = token_iterator->cursor, .size = 0};
}

pair_const_string_slice_t split_const_string_slice_on_token_boundary(
	const_string_slice_t original_slice,
	size_t slice_index
) {
	while(!is_token_delimiter(original_slice.pointer[slice_index]) && slice_index < original_slice.size)
		++slice_index;

	return __unsafe_split_const_string_slice(original_slice, slice_index);
}
