#include "token_frequency_list.h"

static inline void init_token_frequency_node(
	token_frequency_list_node_t *const token_frequency_node,
	const const_token_t token
) {
	token_frequency_node->token = token;
	token_frequency_node->number_occurrences = 1;
}

static inline void init_token_frequency_node_from_frequency_node(
	token_frequency_list_node_t *const token_frequency_node,
	const token_frequency_list_node_t node
) {
	token_frequency_node->token = node.token;
	token_frequency_node->number_occurrences = node.number_occurrences;
}

void expand_token_frequency_list(
	token_frequency_list_t *const token_frequency_list
) {
#define EXPAND_FACTOR 2
	token_frequency_list->capacity *= EXPAND_FACTOR;
	token_frequency_list->tokens =
		reallocate_memory(token_frequency_list->tokens, token_frequency_list->capacity);
#undef EXPAND_FACTOR
}

void add_token_to_frequency_list(
	token_frequency_list_t *const token_frequency_list,
	const const_token_t token
) {
	for (register size_t index = 0; index < token_frequency_list->distinct_token_count; ++index) {
		token_frequency_list_node_t *const token_frequency_node = token_frequency_list->tokens + index;
		if (0 == compare_tokens(token, token_frequency_node->token)) {
			++token_frequency_node->number_occurrences;
			++token_frequency_list->token_count;
			return;
		}
	}

	if (token_frequency_list->capacity == token_frequency_list->distinct_token_count)
		expand_token_frequency_list(token_frequency_list);

	init_token_frequency_node(token_frequency_list->tokens + token_frequency_list->distinct_token_count++, token);
	++token_frequency_list->token_count;
}

void add_token_frequency_list_node_to_frequency_list(
	token_frequency_list_t *const token_frequency_list,
	const token_frequency_list_node_t token_frequency_node_to_add
) {
	for (register size_t index = 0; index < token_frequency_list->distinct_token_count; ++index) {
		token_frequency_list_node_t *const token_frequency_node = token_frequency_list->tokens + index;
		if (0 == compare_tokens(token_frequency_node_to_add.token, token_frequency_node->token)) {
			token_frequency_node->number_occurrences += token_frequency_node_to_add.number_occurrences;
			token_frequency_list->token_count += token_frequency_node_to_add.number_occurrences;
			return;
		}
	}

	if (token_frequency_list->capacity == token_frequency_list->distinct_token_count)
		expand_token_frequency_list(token_frequency_list);

	init_token_frequency_node_from_frequency_node(
		token_frequency_list->tokens + token_frequency_list->distinct_token_count++,
		token_frequency_node_to_add
	);

	token_frequency_list->token_count += token_frequency_node_to_add.number_occurrences;
}

static inline int compare_token_nodes(const void *__token_node1, const void *__token_node2) {
	const token_frequency_list_node_t *const token_node1 = (const token_frequency_list_node_t *const)__token_node1;
	const token_frequency_list_node_t *const token_node2 = (const token_frequency_list_node_t *const)__token_node2;

	return token_node2->number_occurrences - token_node1->number_occurrences;
}

void display_token_frequency_list(const token_frequency_list_t *const token_frequency_list, FILE *const file) {
	qsort(
		token_frequency_list->tokens,
		token_frequency_list->distinct_token_count,
		sizeof(token_frequency_list_node_t),
		compare_token_nodes
	);
	for (register size_t index = 0; index < token_frequency_list->distinct_token_count; ++index) {
		const token_frequency_list_node_t *const token_frequency_node = token_frequency_list->tokens + index;
		double token_frequency = (double)token_frequency_node->number_occurrences / token_frequency_list->token_count;
		fwrite(token_frequency_node->token.pointer, 1, token_frequency_node->token.size, file);
		fprintf(file, " %lf\n", token_frequency);
	}
}
