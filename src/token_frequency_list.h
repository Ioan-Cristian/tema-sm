#ifndef __TOKEN_FREQUENCY_LIST_H__
#define __TOKEN_FREQUENCY_LIST_H__

#include "allocator.h"
#include "token.h"
#include <stddef.h>

typedef struct {
	const_token_t token;
	size_t number_occurrences;
} token_frequency_list_node_t;

typedef struct {
	token_frequency_list_node_t *tokens;
	size_t token_count;
	size_t distinct_token_count;
	size_t capacity;
} token_frequency_list_t;

static inline void init_token_frequency_list(
	token_frequency_list_t *const token_frequency_list
) {
#define TOKEN_LIST_INITIAL_SIZE 4096
	token_frequency_list->tokens =
		(token_frequency_list_node_t *)allocate_memory(TOKEN_LIST_INITIAL_SIZE * sizeof(token_frequency_list_node_t));
	token_frequency_list->capacity = TOKEN_LIST_INITIAL_SIZE;
#undef TOKEN_LIST_INITIAL_SIZE
	token_frequency_list->token_count = 0;
	token_frequency_list->distinct_token_count = 0;
}

void add_token_to_frequency_list(
	token_frequency_list_t *const token_frequency_list,
	const_token_t token
);

void add_token_frequency_list_node_to_frequency_list(
	token_frequency_list_t *const token_frequency_list,
	token_frequency_list_node_t list_node
);

static inline void add_frequency_list_to_frequency_list(
	token_frequency_list_t *const token_frequency_list_destination,
	const token_frequency_list_t *const token_frequency_list_source
) {
	for (
		register size_t index = 0;
		index < token_frequency_list_source->distinct_token_count;
		++index
	)
		add_token_frequency_list_node_to_frequency_list(
			token_frequency_list_destination,
			token_frequency_list_source->tokens[index]
		);
}

static inline token_frequency_list_t new_token_frequency_list_from_token_iterator(token_iterator_t *const token_iterator) {
	token_frequency_list_t token_frequency_list;
	init_token_frequency_list(&token_frequency_list);
	const_token_t token;
	while((token = next_token(token_iterator)).size != 0)
		add_token_to_frequency_list(&token_frequency_list, token);

	return token_frequency_list;
}

void display_token_frequency_list(const token_frequency_list_t *const token_frequency_list, FILE *const file);

static inline void deinit_token_frequency_list(const token_frequency_list_t *const token_frequency_list) {
	free(token_frequency_list->tokens);
}

#endif // __TOKEN_FREQUENCY_LIST_H__
