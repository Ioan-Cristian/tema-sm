#include "program_pthreads.h"

#include <stdint.h>
#include <stdlib.h>

uint8_t main(const unsigned number_arguments, const char *const arguments[]) {
	command_line_arguments_pthreads_t command_line_arguments_pthreads =
		parse_command_line_arguments_pthreads(number_arguments, arguments);

	start_program_pthreads(&command_line_arguments_pthreads);

	exit(EXIT_SUCCESS);
}
