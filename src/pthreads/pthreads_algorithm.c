#include "pthreads_algorithm.h"
#include "pthread.h"
#include "../serial_algorithm.h"
#include "../string_slice_list.h"

typedef struct {
	const_string_slice_t slice;
	token_frequency_list_t token_frequency_list;
} thread_argument_t;

static void *thread_function(void *argument) {
	thread_argument_t *thread_argument = (thread_argument_t *)argument;

	thread_argument->token_frequency_list =
		compute_token_frequency_list_serial(thread_argument->slice);

	return NULL;
}

token_frequency_list_t compute_token_frequency_list_pthreads(
	const const_string_slice_t slice,
	const unsigned number_threads
) {
	const const_string_slice_list_t slice_list = unsafe_partition_slice(slice, number_threads);

	thread_argument_t thread_arguments[__MAX_NUMBER_THREADS__];
	pthread_t threads[__MAX_NUMBER_THREADS__];

	for (
		register unsigned thread_index = 0;
		thread_index < number_threads;
		++thread_index
	) thread_arguments[thread_index].slice = slice_list.slices[thread_index];

	for (
		register unsigned thread_index = 1;
		thread_index < number_threads;
		++thread_index
	) pthread_create(threads + thread_index, NULL, thread_function, thread_arguments + thread_index);

	thread_function(thread_arguments);

	for (
		register unsigned thread_index = 1;
		thread_index < number_threads;
		++thread_index
	) pthread_join(threads[thread_index], NULL);

	for (
	   register unsigned thread_index = 1;
	   thread_index < number_threads;
	   ++thread_index
	) {
	   add_frequency_list_to_frequency_list(
					   &thread_arguments->token_frequency_list,
					   &thread_arguments[thread_index].token_frequency_list
	   );
	   deinit_token_frequency_list(&thread_arguments[thread_index].token_frequency_list);
	}

	return thread_arguments->token_frequency_list;
}
