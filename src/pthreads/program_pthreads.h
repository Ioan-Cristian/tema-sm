#ifndef __PROGRAM_PTHREADS_H__
#define __PROGRAM_PTHREADS_H__

#include "command_line_pthreads.h"

void start_program_pthreads(const command_line_arguments_pthreads_t *const command_line_arguments_pthreads);

#endif // __PROGRAM_PTHREADS_H__
