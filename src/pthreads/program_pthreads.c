#include "../config.h"
#include "../file.h"
#include "program_pthreads.h"
#include "pthreads_algorithm.h"

void start_program_pthreads(const command_line_arguments_pthreads_t *const command_line_arguments_pthreads) {
	const const_string_slice_t input_file_content = get_file_content(command_line_arguments_pthreads->file_name);

	const token_frequency_list_t token_frequency_list =
		compute_token_frequency_list_pthreads(input_file_content, command_line_arguments_pthreads->number_threads);

	display_token_frequency_list(&token_frequency_list, stdout);
	deinit_token_frequency_list(&token_frequency_list);

	free((void *)input_file_content.pointer);
}
