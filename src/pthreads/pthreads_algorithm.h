#ifndef __PTHREADS_ALGORITHM_H__
#define __PTHREADS_ALGORITHM_H__

#include "../token_frequency_list.h"

token_frequency_list_t compute_token_frequency_list_pthreads(
	const const_string_slice_t slice,
	const unsigned number_threads
);

#endif // __PTHREADS_ALGORITHM_H__
