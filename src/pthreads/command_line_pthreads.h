#ifndef __COMMAND_LINE_PTHREADS_H__
#define __COMMAND_LINE_PTHREADS_H__

#include "../command_line.h"

typedef command_line_arguments_t command_line_arguments_pthreads_t;

#define __parse_command_line_arguments_pthreads(\
		number_arguments,\
		arguments,\
		compilation_unit,\
		line_number,\
		function_name\
) __parse_command_line_arguments(number_arguments, arguments, compilation_unit, line_number, function_name)

#define parse_command_line_arguments_pthreads(number_arguments, arguments) \
	__parse_command_line_arguments_pthreads(number_arguments, arguments, __FILE__, __LINE__, __func__)

#endif // __COMMAND_LINE_PTHREADS_H__
