#ifndef __SERIAL_ALGORITHM_H__
#define __SERIAL_ALGORITHM_H__

#include "token_frequency_list.h"

static inline token_frequency_list_t compute_token_frequency_list_serial(const const_string_slice_t input_file_content) {
	token_iterator_t token_iterator = new_token_iterator(input_file_content);
	return new_token_frequency_list_from_token_iterator(&token_iterator);
}

#endif // __SERIAL_ALGORITHM_H__
