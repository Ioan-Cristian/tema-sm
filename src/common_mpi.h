#ifndef __COMMON_MPI_H__
#define __COMMON_MPI_H__

#include "string_slice_list.h"
#include "token_frequency_list.h"

#include "mpi.h"

#define TAG 0

static inline void send_slice(const const_string_slice_t slice, const unsigned receiver) {
	const size_t slice_size = slice.size;
	MPI_Send(&slice_size, 1, MPI_LONG, receiver, TAG, MPI_COMM_WORLD);
	MPI_Send(slice.pointer, slice.size, MPI_CHAR, receiver, TAG, MPI_COMM_WORLD);
}

static inline const_string_slice_t receive_slice(const unsigned sender) {
	size_t slice_size;
	MPI_Recv(&slice_size, 1, MPI_LONG, sender, TAG, MPI_COMM_WORLD, NULL);

	char *pointer = (char *)allocator(slice_size * sizeof(char));

	MPI_Recv(pointer, slice_size, MPI_CHAR, sender, TAG, MPI_COMM_WORLD, NULL);

	return (const_string_slice_t){
		.pointer = pointer,
		.size = slice_size
	};
}

typedef struct {
	size_t token_count;
	size_t distinct_token_count;
	size_t capacity;
} token_frequency_list_parameters_t;

static inline void send_token_frequency_list_parameters(
	const token_frequency_list_t *const token_frequency_list,
	const unsigned receiver
) {
	MPI_Send(&token_frequency_list->token_count, 3, MPI_LONG, receiver, TAG, MPI_COMM_WORLD);
}

static inline void send_token(const const_token_t token, const unsigned receiver) {
	send_slice(token, receiver);
}

static inline void send_token_node(
	const token_frequency_list_node_t *const token,
	const unsigned receiver
) {
	send_token(token->token, receiver);
	MPI_Send(&token->number_occurrences, 1, MPI_LONG, receiver, TAG, MPI_COMM_WORLD);
}

static inline void send_token_nodes(
	const token_frequency_list_node_t *const tokens,
	const size_t number_tokens,
	const unsigned receiver
) {
	for (
		register size_t token_index = 0;
		token_index < number_tokens;
		++token_index
	)
		send_token_node(tokens + token_index, receiver);
}

static inline void send_token_frequency_list(
	const token_frequency_list_t *const token_frequency_list,
	const unsigned receiver
) {
	send_token_frequency_list_parameters(token_frequency_list, receiver);
	send_token_nodes(token_frequency_list->tokens, token_frequency_list->distinct_token_count, receiver);
}

static inline token_frequency_list_parameters_t receive_token_frequency_list_parameters(
	const unsigned sender
) {
	token_frequency_list_parameters_t token_frequency_list_parameters;
	MPI_Recv(&token_frequency_list_parameters, 3, MPI_LONG, sender, TAG, MPI_COMM_WORLD, 0);

	return token_frequency_list_parameters;
}

static inline const_token_t receive_token(const unsigned sender) {
	return receive_slice(sender);
}

static inline void receive_token_node(
	token_frequency_list_node_t *const token_node,
	const unsigned sender
) {
	token_node->token = receive_token(sender);
	MPI_Recv(&token_node->number_occurrences, 1, MPI_LONG, sender, TAG, MPI_COMM_WORLD, NULL);
}

static inline token_frequency_list_node_t *receive_token_nodes(
	const unsigned number_tokens,
	const unsigned sender
) {
	token_frequency_list_node_t *const token_nodes =
		(token_frequency_list_node_t *)allocator(number_tokens * sizeof(token_frequency_list_node_t));

	for (
		register size_t token_index = 0;
		token_index < number_tokens;
		++token_index
	)
		receive_token_node(token_nodes + token_index, sender);

	return token_nodes;
}

static inline token_frequency_list_t receive_token_frequency_list(
       const unsigned sender
) {
       const token_frequency_list_parameters_t token_frequency_list_parameters =
               receive_token_frequency_list_parameters(sender);

       token_frequency_list_node_t *token_nodes =
               receive_token_nodes(token_frequency_list_parameters.distinct_token_count, sender);

       return (token_frequency_list_t){
               .tokens = token_nodes,
               .token_count = token_frequency_list_parameters.token_count,
               .distinct_token_count = token_frequency_list_parameters.distinct_token_count,
               // the previous call to receive_token_nodes allocates exactly
               // token_frequency_list_parameters->distinct_token_count token nodes
               .capacity = token_frequency_list_parameters.distinct_token_count
       };
}

#undef TAG

#endif // __COMMON_MPI_H__
