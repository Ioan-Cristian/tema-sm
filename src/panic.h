#ifndef __PANIC_H__
#define __PANIC_H__

#include <stdio.h>
#include <stdlib.h>

#define __panic(file, line, function, ...) \
	do { \
		fprintf(stderr, "%s:%u: in %s():\n\x1B[1;31merror:\x1B[m ", file, line, function); \
		fprintf(stderr, __VA_ARGS__); \
		exit(EXIT_FAILURE); \
	} while(0)

#define panic(...) \
	__panic(__FILE__, __LINE__, __func__, __VA_ARGS__)

#endif // __PANIC_H__
