#include "allocator.h"
#include "file.h"

static inline unsigned long __get_file_position(
	FILE *const file,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	const long file_position = ftell(file);
	if (-1 == file_position) __panic(compilation_unit, line_number, function_name, "error while getting the file position");

	return (size_t)file_position;
}

static size_t __get_file_size(
	FILE *const file,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	const size_t current_file_position = __get_file_position(file, compilation_unit, line_number, function_name);
	fseek(file, 0, SEEK_END);
	const size_t file_size = __get_file_position(file, compilation_unit, line_number, function_name);
	fseek(file, current_file_position, SEEK_SET);

	return file_size;
}

void __read_bytes(
	char *const bytes,
	const size_t number_of_bytes,
	FILE *const file,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	if (number_of_bytes != fread(bytes, 1, number_of_bytes, file))
		__panic(compilation_unit, line_number, function_name, "couldn't read %zu bytes\n", number_of_bytes);
}

const_string_slice_t __read_file(
	FILE *const file,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	const size_t file_size = __get_file_size(file, compilation_unit, line_number, function_name);

	char *const file_content = (char *)allocate_memory(file_size);
	__read_bytes(file_content, file_size, file, compilation_unit, line_number, function_name);

	return (const_string_slice_t){.pointer = file_content, .size = file_size};
}
