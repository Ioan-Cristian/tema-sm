#ifndef __PROGRAM_H__
#define __PROGRAM_H__

#include "config.h"

#if __ALGORITHM__ == __SERIAL_ALGORITHM__
#include "serial/program_serial.h"
#elif __ALGORITHM__ == __OPENMP_ALGORITHM__
#include "openmp/programp_openmp.h"
#elif __ALGORITHM__ == __MPI_ALGORITHM__
#include "mpi/program_mpi.h"
#endif

#endif // __PROGRAM_H__
