#ifndef __PROGRAM_SERIAL_H__
#define __PROGRAM_SERIAL_H__

#include "command_line_serial.h"

void start_program_serial(const command_line_arguments_serial_t *const command_line_arguments_serial);

#endif // __PROGRAM_SERIAL_H__
