#include "program_serial.h"

#include <stdint.h>
#include <stdlib.h>

uint8_t main(const unsigned number_arguments, const char *const arguments[]) {
	command_line_arguments_serial_t command_line_arguments_serial =
		parse_command_line_arguments_serial(number_arguments, arguments);

	start_program_serial(&command_line_arguments_serial);

	return EXIT_SUCCESS;
}
