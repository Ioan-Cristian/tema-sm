#include "algorithm_serial.h"
#include "../file.h"
#include "program_serial.h"
#include "../token_frequency_list.h"

void start_program_serial(const command_line_arguments_serial_t *const command_line_arguments_serial) {
	const_string_slice_t input_file_content = get_file_content(command_line_arguments_serial->file_name);

	token_frequency_list_t token_frequency_list =
		compute_token_frequency_list_serial(input_file_content);
	display_token_frequency_list(&token_frequency_list, stdout);
	deinit_token_frequency_list(&token_frequency_list);

	free((void *)input_file_content.pointer);
}
