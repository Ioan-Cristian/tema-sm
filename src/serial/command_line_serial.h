#ifndef __COMMAND_LINE_SERIAL_H__
#define __COMMAND_LINE_SERIAL_H__

typedef struct {
	const char *const file_name;
} command_line_arguments_serial_t;

#include "../panic.h"

static inline command_line_arguments_serial_t __parse_command_line_arguments_serial(
	const unsigned number_arguments,
	const char *const arguments[],
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	if (number_arguments != 2)
		__panic(
			compilation_unit,
			line_number,
			function_name,
			"The command requires one argument:\n1. The name of the input file\n"
		);

	return (command_line_arguments_serial_t){
		.file_name = arguments[1]
	};
}

#define parse_command_line_arguments_serial(number_arguments, arguments) \
	__parse_command_line_arguments_serial(number_arguments, arguments, __FILE__, __LINE__, __func__)

#endif // __COMMAND_LINE_SERIAL_H__
