#include "program_openmp.h"

#include <stdint.h>
#include <stdlib.h>

uint8_t main(const unsigned number_arguments, const char *const arguments[]) {
	command_line_arguments_openmp_t command_line_arguments_openmp =
		parse_command_line_arguments_openmp(number_arguments, arguments);

	start_program_openmp(&command_line_arguments_openmp);

	return EXIT_SUCCESS;
}
