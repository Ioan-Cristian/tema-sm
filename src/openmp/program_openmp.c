#include "openmp_algorithm.h"
#include "../file.h"
#include "program_openmp.h"
#include "../token_frequency_list.h"

void start_program_openmp(const command_line_arguments_openmp_t *const command_line_arguments_openmp) {
	const_string_slice_t input_file_content = get_file_content(command_line_arguments_openmp->file_name);

	token_frequency_list_t token_frequency_list =
		compute_token_frequency_list_openmp(input_file_content, command_line_arguments_openmp->number_threads);
	display_token_frequency_list(&token_frequency_list, stdout);
	deinit_token_frequency_list(&token_frequency_list);

	free((void *)input_file_content.pointer);
}
