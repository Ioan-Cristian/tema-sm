#ifndef __PROGRAM_OPENMP_H__
#define __PROGRAM_OPENMP_H__

#include "command_line_openmp.h"

void start_program_openmp(const command_line_arguments_openmp_t *const command_line_arguments_openmp);

#define start_program(command_line_arguments) \
	start_program_openmp(command_line_arguments)

#endif // __PROGRAM_OPENMP_H__
