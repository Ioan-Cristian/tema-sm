#ifndef __OPENMP_ALGORITHM_H__
#define __OPENMP_ALGORITHM_H__

#include "../token_frequency_list.h"

token_frequency_list_t compute_token_frequency_list_openmp(
	const const_string_slice_t input_file_content,
	const unsigned number_threads
);

#endif // __OPENMP_ALGORITHM_H__
