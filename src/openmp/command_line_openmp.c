#include "command_line_openmp.h"
#include "../panic.h"
#include "../utils.h"

static inline unsigned parse_number_threads(
	const char *const string_number,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	const long number = __parse_number(string_number, compilation_unit, line_number, function_name);

	if (number <= 0)
		__panic(
			compilation_unit,
			line_number,
			function_name,
			"%ld is not a valid thread number",
			number
		);

	return (unsigned)number;
}

command_line_arguments_openmp_t __parse_command_line_arguments_openmp(
	const unsigned number_arguments,
	const char *const arguments[],
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	if (number_arguments != 3)
		__panic(
			compilation_unit,
			line_number,
			function_name,
			"The command requires two arguments:\n1. The name of the input file\n2. The number of threads\n"
		);

	return (command_line_arguments_openmp_t){
		.file_name = arguments[1],
		.number_threads = parse_number_threads(arguments[2], compilation_unit, line_number, function_name)
	};
}
