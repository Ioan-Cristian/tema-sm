#ifndef __COMMAND_LINE_OPENMP_H__
#define __COMMAND_LINE_OPENMP_H__

typedef struct {
	const char *const file_name;
	const unsigned number_threads;
} command_line_arguments_openmp_t;

command_line_arguments_openmp_t __parse_command_line_arguments_openmp(
	const unsigned number_arguments,
	const char *const arguments[],
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
);

#define parse_command_line_arguments_openmp(number_arguments, arguments) \
	__parse_command_line_arguments_openmp(number_arguments, arguments, __FILE__, __LINE__, __func__)

#endif // __COMMAND_LINE_OPENMP_H__
