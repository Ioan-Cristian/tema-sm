#include "openmp_algorithm.h"
#include "../string_slice_list.h"

#include "omp.h"

token_frequency_list_t compute_token_frequency_list_openmp(
	const const_string_slice_t input_file_content,
	const unsigned number_threads
) {
	omp_set_num_threads(number_threads);
	const_string_slice_list_t slice_list = unsafe_partition_slice(input_file_content, number_threads);
	token_frequency_list_t token_frequency_lists[__MAX_NUMBER_THREADS__];
#pragma omp parallel for
	for (
		register unsigned thread_index = 0;
		thread_index < number_threads;
		++thread_index
	) {
		token_iterator_t token_iterator = new_token_iterator(slice_list.slices[thread_index]);
		token_frequency_lists[thread_index] = new_token_frequency_list_from_token_iterator(&token_iterator);
	}

	for (
		register unsigned level = 1;
		level < number_threads;
		level <<= 1
	) {
		const unsigned threshold = number_threads - level;
#pragma omp parallel for
		for (register unsigned thread_index = 0; thread_index < threshold; thread_index += level << 1) {
			add_frequency_list_to_frequency_list(token_frequency_lists + thread_index, token_frequency_lists + thread_index + level);
			deinit_token_frequency_list(token_frequency_lists + thread_index + level);
		}
	}

	return token_frequency_lists[0];
}
