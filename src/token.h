#ifndef __TOKEN_H__
#define __TOKEN_H__

#include "string_slice.h"

#include <string.h>

typedef const_string_slice_t const_token_t;

static inline int compare_tokens(const_token_t token1, const_token_t token2) {
	if (token1.size != token2.size) return token1.size - token2.size;
	return memcmp(token1.pointer, token2.pointer, token1.size);
}

typedef struct {
	const char *cursor;
	const char *end;
} token_iterator_t;

static inline token_iterator_t new_token_iterator(const_string_slice_t const_string_slice) {
	return (token_iterator_t){
		.cursor = const_string_slice.pointer,
		.end = const_string_slice.pointer + const_string_slice.size
	};
}

const_token_t next_token(token_iterator_t *const token_iterator);

pair_const_string_slice_t split_const_string_slice_on_token_boundary(
	const_string_slice_t original_slice,
	size_t slice_index
);

#endif // __TOKEN_H__
