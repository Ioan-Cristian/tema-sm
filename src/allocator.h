#ifndef __ALLOCATOR_H__
#define __ALLOCATOR_H__

#include "panic.h"

#include <stdlib.h>

#define allocator(size) malloc(size)

static inline void *__allocate_memory(
	const size_t size,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	void *memory_pointer = allocator(size);
	if (!memory_pointer)
		__panic(compilation_unit, line_number, function_name, "couldn't allocate memory of size %zu\n", size);

	return memory_pointer;
}

#define allocate_memory(size) \
	__allocate_memory(size, __FILE__, __LINE__, __func__)

#define reallocator(pointer, size) realloc(pointer, size)

static inline void *__reallocate_memory(
	void *pointer,
	const size_t size,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	void *new_pointer = reallocator(pointer, size);
	if (!new_pointer)
		__panic(compilation_unit, line_number, function_name, "couldn't reallocate memory of size %zu\n", size);

	return new_pointer;
}

#define reallocate_memory(pointer, element_count) \
	__reallocate_memory(pointer, element_count * sizeof(*pointer), __FILE__, __LINE__, __func__)

#endif // __ALLOCATOR_H__
