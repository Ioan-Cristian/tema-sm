#ifndef __PAIR_H__
#define __PAIR_H__

#define define_pair(name, type1, type2) \
typedef struct { \
	type1 first; \
	type2 second; \
} name;

#endif // __PAIR_H__
