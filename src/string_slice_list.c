#include "string_slice_list.h"
#include "token.h"

const_string_slice_list_t unsafe_partition_slice(
	const_string_slice_t slice,
	const unsigned number_threads
) {
	const_string_slice_list_t slice_list = {.size = number_threads};
	const unsigned chunk_size = slice.size / number_threads;
	pair_const_string_slice_t pair = {.first = slice, .second = slice};
	for (
		register unsigned thread_index = 0, number_threads_minus_1 = number_threads - 1;
		thread_index < number_threads_minus_1;
		++thread_index
	) {
		pair = split_const_string_slice_on_token_boundary(pair.second, chunk_size);
		slice_list.slices[thread_index] = pair.first;
	}
	slice_list.slices[number_threads - 1] = pair.second;

	return slice_list;
}
