#ifndef __MPI_PROGRAM_H__
#define __MPI_PROGRAM_H__

#include "command_line_mpi.h"

void start_program_mpi(const command_line_arguments_mpi_t *const command_line_arguments_mpi);

#define start_program(command_line_arguments) \
	start_program_mpi(command_line_arguments)

#endif // __MPI_PROGRAM_H__
