#ifndef __MPI_ALGORITHM_H__
#define __MPI_ALGORITHM_H__

#include "../token_frequency_list.h"

token_frequency_list_t compute_token_frequency_list_mpi(const const_string_slice_t subslice);

#endif // __MPI_ALGORITHM_H__
