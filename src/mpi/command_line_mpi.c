#include "command_line_mpi.h"
#include "../panic.h"

#include "mpi.h"

command_line_arguments_mpi_t __parse_command_line_arguments_mpi(
	const unsigned number_arguments,
	const char *const arguments[],
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	MPI_Init((int *)&number_arguments, (char ***)&arguments);

	if (number_arguments != 2)
		__panic(
			compilation_unit,
			line_number,
			function_name,
			"The command requires one argument:\n1. The name of the input file\n"
		);

	unsigned number_processes;
	MPI_Comm_size(MPI_COMM_WORLD, (int *)&number_processes);
	unsigned rank;
	MPI_Comm_rank(MPI_COMM_WORLD, (int *)&rank);

	return (command_line_arguments_mpi_t){
		.file_name = arguments[1],
		.number_processes = number_processes,
		.rank = rank,
		.master_rank = 0
	};
}
