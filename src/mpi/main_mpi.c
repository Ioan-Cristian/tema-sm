#include "program_mpi.h"

#include "mpi.h"

#include <stdint.h>
#include <stdlib.h>

uint8_t main(const unsigned number_arguments, const char *const arguments[]) {
	command_line_arguments_mpi_t command_line_arguments_mpi =
		parse_command_line_arguments_mpi(number_arguments, arguments);

	start_program_mpi(&command_line_arguments_mpi);

	MPI_Finalize();
	exit(EXIT_SUCCESS);
}
