#include "mpi_algorithm.h"

token_frequency_list_t compute_token_frequency_list_mpi(const const_string_slice_t subslice) {
	token_iterator_t token_iterator = new_token_iterator(subslice);
	return new_token_frequency_list_from_token_iterator(&token_iterator);
}
