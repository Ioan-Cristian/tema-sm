#ifndef __STRING_SLICE_H__
#define __STRING_SLICE_H__

#include "pair.h"

#include <stddef.h>

typedef struct {
	const char *pointer;
	size_t size;
} const_string_slice_t;

define_pair(pair_const_string_slice_t, const_string_slice_t, const_string_slice_t)

#include "panic.h"

static inline pair_const_string_slice_t __unsafe_split_const_string_slice(
	const_string_slice_t original_slice,
	const size_t split_index
) {
	return (pair_const_string_slice_t){
		.first = (const_string_slice_t){
			.pointer = original_slice.pointer,
			.size = split_index
		},
		.second = (const_string_slice_t){
			.pointer = original_slice.pointer + split_index,
			.size = original_slice.size - split_index
		}
	};
}

static inline pair_const_string_slice_t __split_const_string_slice(
	const_string_slice_t original_slice,
	const size_t split_index,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	if (split_index > original_slice.size)
		__panic(
			compilation_unit,
			line_number,
			function_name,
			"split index %zu out of bound %zu\n",
			split_index, original_slice.size
		);

	return __unsafe_split_const_string_slice(original_slice, split_index);
}

#define split_const_string_slice(original_slice, split_index) \
	__split_const_string_slice(original_slice, split_index, __FILE__, __LINE__, __func__)

#endif // __STRING_SLICE_H__
