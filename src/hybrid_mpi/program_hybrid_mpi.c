#include "../common_mpi.h"
#include "../file.h"
#include "hybrid_mpi_algorithm.h"
#include "program_hybrid_mpi.h"

static void do_master(const command_line_arguments_hybrid_mpi_t *const command_line_arguments_hybrid_mpi) {
	const const_string_slice_t input_file_content = get_file_content(command_line_arguments_hybrid_mpi->file_name);

	const const_string_slice_list_t slice_list =
		unsafe_partition_slice(input_file_content, command_line_arguments_hybrid_mpi->number_processes);

	for(
		register unsigned process_index = 0;
		process_index < command_line_arguments_hybrid_mpi->master_rank;
		++process_index
	)
		send_slice(slice_list.slices[process_index], process_index);

	for(
		register unsigned process_index = command_line_arguments_hybrid_mpi->master_rank + 1;
		process_index < command_line_arguments_hybrid_mpi->number_processes;
		++process_index
	)
		send_slice(slice_list.slices[process_index], process_index);

	const const_string_slice_t slice = slice_list.slices[command_line_arguments_hybrid_mpi->master_rank];

	token_frequency_list_t token_frequency_list =
		compute_token_frequency_list_hybrid_mpi(slice, command_line_arguments_hybrid_mpi->number_threads);

	for(
		register unsigned process_index = 0;
		process_index < command_line_arguments_hybrid_mpi->master_rank;
		++process_index
	) {
		const token_frequency_list_t partial_token_frequency_list =
			receive_token_frequency_list(process_index);
		add_frequency_list_to_frequency_list(&token_frequency_list, &partial_token_frequency_list);
		deinit_token_frequency_list(&partial_token_frequency_list);
	}

	for(
		register unsigned process_index = command_line_arguments_hybrid_mpi->master_rank + 1;
		process_index < command_line_arguments_hybrid_mpi->number_processes;
		++process_index
	) {
		const token_frequency_list_t partial_token_frequency_list =
			receive_token_frequency_list(process_index);
		add_frequency_list_to_frequency_list(&token_frequency_list, &partial_token_frequency_list);
		deinit_token_frequency_list(&partial_token_frequency_list);
	}

	display_token_frequency_list(&token_frequency_list, stdout);
	deinit_token_frequency_list(&token_frequency_list);

	free((void *)input_file_content.pointer);
}

static void do_worker(const command_line_arguments_hybrid_mpi_t *const command_line_arguments_hybrid_mpi) {
	const const_string_slice_t slice = receive_slice(command_line_arguments_hybrid_mpi->master_rank);

	token_frequency_list_t token_frequency_list =
		compute_token_frequency_list_hybrid_mpi(slice, command_line_arguments_hybrid_mpi->number_threads);
	send_token_frequency_list(&token_frequency_list, command_line_arguments_hybrid_mpi->master_rank);
	deinit_token_frequency_list(&token_frequency_list);

	free((void *)slice.pointer);
}

void start_program_hybrid_mpi(
	const command_line_arguments_hybrid_mpi_t *const command_line_arguments_hybrid_mpi
) {
	if (command_line_arguments_hybrid_mpi->rank == command_line_arguments_hybrid_mpi->master_rank)
		do_master(command_line_arguments_hybrid_mpi);
	else do_worker(command_line_arguments_hybrid_mpi);
}
