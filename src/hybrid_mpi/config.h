#ifndef __CONFIG_HYBRID_MPI_T__
#define __CONFIG_HYBRID_MPI_T__

#define __MASTER_RANK__ 0

#define __OPENMP__ 0
#define __PTHREADS__ 1
#define __ALGORITHM__ __OPENMP__

#endif // __CONFIG_HYBRID_MPI_T__
