#ifndef __PROGRAM_HYBRID_MPI_H__
#define __PROGRAM_HYBRID_MPI_H__

#include "command_line_hybrid_mpi.h"

void start_program_hybrid_mpi(
	const command_line_arguments_hybrid_mpi_t *const command_line_arguments_hybrid_mpi
);

#endif // __PROGRAM_HYBRID_MPI_H__
