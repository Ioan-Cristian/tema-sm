#ifndef __HYBRID_MPI_ALGORITHM_H__
#define __HYBRID_MPI_ALGORITHM_H__

#include "config.h"
#include "../openmp/openmp_algorithm.h"
#include "../pthreads/pthreads_algorithm.h"

static inline token_frequency_list_t compute_token_frequency_list_hybrid_mpi(
	const const_string_slice_t subslice,
	const unsigned number_threads
) {
#ifndef __ALGORITHM__
#error "Forgot to include config.h"
#else
#if __ALGORITHM__ == __OPENMP__
	return compute_token_frequency_list_openmp(subslice, number_threads);
#elif __ALGORITHM__ == __PTHREADS__
	return compute_token_frequency_list_pthreads(subslice, number_threads);
#else
#error "Unknown algorithm"
#endif
#endif
}

#endif // __HYBRID_MPI_ALGORITHM_H__
