#include "program_hybrid_mpi.h"

#include "mpi.h"

#include <stdint.h>
#include <stdlib.h>

uint8_t main(const unsigned number_arguments, const char *const arguments[]) {
	const command_line_arguments_hybrid_mpi_t command_line_arguments_hybrid_mpi =
		parse_command_line_arguments_hybrid_mpi(number_arguments, arguments);

	start_program_hybrid_mpi(&command_line_arguments_hybrid_mpi);

	MPI_Finalize();
	exit(EXIT_SUCCESS);
}
