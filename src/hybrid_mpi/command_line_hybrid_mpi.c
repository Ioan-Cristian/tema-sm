#include "command_line_hybrid_mpi.h"
#include "../command_line.h"
#include "config.h"
#include "../panic.h"

#include "mpi.h"

command_line_arguments_hybrid_mpi_t __parse_command_line_arguments_hybrid_mpi(
	const unsigned number_arguments,
	const char *const arguments[],
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	MPI_Init((int *)&number_arguments, (char ***)&arguments);

	const command_line_arguments_t command_line_arguments =
		__parse_command_line_arguments(
			number_arguments,
			arguments,
			compilation_unit,
			line_number,
			function_name
		);

	unsigned number_processes;
	MPI_Comm_size(MPI_COMM_WORLD, (int *)&number_processes);
	unsigned rank;
	MPI_Comm_rank(MPI_COMM_WORLD, (int *)&rank);

	return (command_line_arguments_hybrid_mpi_t){
		.file_name = command_line_arguments.file_name,
		.number_processes = number_processes,
		.number_threads = command_line_arguments.number_threads,
		.rank = rank,
		.master_rank = __MASTER_RANK__
	};
}
