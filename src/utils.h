#ifndef __UTILS_H__
#define __UTILS_H__

static inline long __parse_number(
	const char *const string_number,
	const char *const compilation_unit,
	const unsigned line_number,
	const char *const function_name
) {
	char *bad_character;
	const long number = strtol(string_number, &bad_character, 10);

	if (*bad_character != '\0')
		__panic(
			compilation_unit,
			line_number,
			function_name,
			"%s is not a number",
			string_number
		);

	return number;
}

#endif // __UTILS_H__
