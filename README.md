# Frecvență cuvinte OpenMP

## Introducere

Acest proiect conține implementarea temei de la SM.

## Descrierea problemei

Dându-se un fișier text cu cuvinte în limba engleză, se dorește determinarea frecvenției de apariție
a fiecărui cuvânt prezent în text.

## Implementare

Proiectul acesta conține următoarele implementări:

1. Algoritmul serial
2. Algoritmul OpenMP: implementare paralelă folosind OpenMP
3. Algoritmul MPI: implementarea paralelă folosind MPI
4. Algoritmul Pthreads: implementarea paralelă folosind Pthreads
5. Algoritmul MPI hibrid: implementarea paralelă folosind MPI și OpenMP/Pthreads

## Lista de unelte necesare compilării

+ `make`
+ `gcc`
+ `mpicc` și `mpirun`

Folosiți gestionarul de pachete al distribuției dumneavoastră pentru a le instala.

## Depedențe

+ `libc`

Folosiți gestionarul de pachete al distribuției dumneavoastră pentru a instala biblioteca standard C
dacă aceasta lipsește.

## Construire

Odată descărcată arhiva cu codul sursă, pentru a construi binarele trebuie să se ruleze următoarele
comenzi:

```bash
make init
make build
```

Binarele construite vor fi plasate în dosarul `bin`.

Notă: pentru MPI hibrid, alegerea între OpenMP și Pthreads se face prin modificarea fișierului de
configurare `src/hybrid_mpi/config.h`

## Măsurarea vitezei de execuție

Măsurarea vitezei de execuție se poate face cu următoarea comandă, după inițializarea proiectului:

```bash
INPUT_FILE=your_input_file.txt NUMBER_THEADS=number_threads NUMBER_NODES=number_nodes make benchmark > results.txt
```

Ordinea rezultatelor afișate:

1. Serial
2. OpenMP
3. MPI
4. Pthreads
5. MPI hibrid

Pe calculatorul personal, cu fișier de intrare Biblia Catolica în limba engleză, 8 fire de execuție
4 noduri și procesor AMD Ryzen 3750H s-au obținut următoarele rezultate (OpenMP pentru MPI hibrid):

```text
real	0m5,431s
user	0m5,407s
sys	0m0,008s

real	0m1,610s
user	0m6,719s
sys	0m0,007s

real	0m4,509s
user	0m5,313s
sys	0m2,366s

real	0m2,029s
user	0m6,248s
sys	0m0,013s

real	0m3,999s
user	0m7,692s
sys	0m2,371s
```

Se observă că OpenMP este cel mai rapid, urmat de Pthreads, apoi de MPI hibrid, MPI și serial.
OpenMP este cel mai rapid pentru că este foarte bine optimizat de către o echipă mondială de
programatori. Pthreads este ceva mai încet, probabil datorită unei implementări mai slabe.
Algoritmii pe bază de MPI sunt mult mai lenți datorită faptului că lista de frecvență trebuie
agregată, ceea ce duce la copierea multor date de la nodurile muncitoare către nodul principal
pentru combinarea listelor de frecvență parțiale. Fenomenul acesta nu se observă la OpenMP și
Pthreads pentru că memoria este partajată între firele de execuție, deci nu-i necesară copierea
datelor. În final algoritmul serial este cel mai lent pentru că nu se folosește de puterea de calcul
paralelă a procesorului.

## Concluzii

În urma realizării proiectului, se observă că implementarea algoritmilor paraleli pe arhitecturi
care permit calcul paralel poate duce la îmbunătățiri de viteză drastice.

OpenMP pare soluția cea mai avantajoasă: extrem de simplu de implementat și performanțe ridicate.
MPI trebuie evitat atunci când este necesară agregarea foarte multor date. În acest caz, se preferă
folosirea unui sistem cu memorie foarte multă (128+GB RAM), pentru a profita de avantajul memoriei
partajate între firele de execuție și evitarea copierii datelor dintr-o parte în alta.

## Probleme

Probleme? Observații? Deschideți un tichet la adresa următoare: https://codeberg.org/Ioan-Cristian/tema-sm
